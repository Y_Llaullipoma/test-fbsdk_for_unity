﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class FBholder : MonoBehaviour
{
    public GameObject FB_notLogged;
    public GameObject FB_logged;
    public GameObject userName_;
    public GameObject userPicture_;

    void Awake() {
        FBManager.Instance.InitFB();
        DealWithFBMenus(FB.IsLoggedIn);
    }

    public void FBlogin() {

        List<string> permissions = new List<string>() {"public_profile", "email", "user_friends" };

        FB.LogInWithReadPermissions(permissions, AuthCallBack);
    }

    void AuthCallBack(IResult result) {

        if (result.Error != null) {
            Debug.Log(result.Error);
        }
        else {
            if (FB.IsLoggedIn) {
                FBManager.Instance.IsLoggedIn = true;
                FBManager.Instance.GetProfile();
                Debug.Log("FB is logged in");
            }
            else {
                Debug.Log("FB is not logged in");
            }
            DealWithFBMenus(FB.IsLoggedIn);
        }
    }

    public void LogoutFB() {
        RestartProfile();
        FB.LogOut();
    }

    void RestartProfile() {
        Image defaultProfilePic = userPicture_.GetComponent<Image>();
        defaultProfilePic.sprite = null;
        Text defaultText = userName_.GetComponent<Text>();
        defaultText.text = "Place holder";
    }

    void DealWithFBMenus(bool isLoggedIn) {

        if (isLoggedIn) {
            FB_logged.SetActive(true);
            FB_notLogged.SetActive(false);

            if (FBManager.Instance.ProfileName != null) {
                Text UserName = userName_.GetComponent<Text>();
                UserName.text = "Hi, " + FBManager.Instance.ProfileName;
            }
            else {
                StartCoroutine("WaitForProfileName");
            }
            if (FBManager.Instance.ProfilePic != null) {
                Image ProfilePic = userPicture_.GetComponent<Image>();
                ProfilePic.sprite = FBManager.Instance.ProfilePic;

            }
            else {
                StartCoroutine("WaitForProfilePic");
            }
        }
        else {
            FB_logged.SetActive(false);
            FB_notLogged.SetActive(true);
        }

    }

    IEnumerator WaitForProfileName() {

        while (FBManager.Instance.ProfileName == null) {
            yield return null;
        }

        DealWithFBMenus(FB.IsLoggedIn);

    }

    IEnumerator WaitForProfilePic() {

        while (FBManager.Instance.ProfilePic == null) {
            yield return null;
        }

        DealWithFBMenus(FB.IsLoggedIn);

    }
    
    public void Share() {
        FBManager.Instance.Share();
    }

    public void FacebookGR() {
        FBManager.Instance.FacebookGameRequest();
    }

    /*
    public void Invite() {
        FBManager.Instance.Invite();
    }

    public void ShareWithUsers() {
        FBManager.Instance.ShareWithUsers();
    }*/

}
